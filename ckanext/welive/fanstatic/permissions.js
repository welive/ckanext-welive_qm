"use strict";

ckan.module('welive', function ($, _) {
  return {
    initialize: function () {

      $("#permissions-edit-form").submit(function() {
          var permissions = {
             "select" : createEntries("select"),
             "delete" : createEntries("delete"),
             "insert" : createEntries("insert"),
             "update" : createEntries("update")
          };

          $("#permissions").val(JSON.stringify(permissions));

          return true;
      });

      var infoData = {};

      var pkgId = $(this.el).attr("pkg-id");
      var resourceId = $(this.el).attr("resource-id");
      var weliveApi = $("#welive_api").val()

      var userData = JSON.parse($("#user_list").val());

      var tableCounter = 0;

      console.log(weliveApi);
      var infoURL = weliveApi + '/ods/' + pkgId + '/resource/' + resourceId + '/mapping/info';
      $.get(infoURL, function(data) {
        infoData = data;

        var permissions = JSON.parse($("#permissions").val());
        if ("select" in permissions) {
          processPermissions("select", permissions["select"]);
        }

        if ("delete" in permissions) {
          processPermissions("delete", permissions["delete"]);
        }

        if ("insert" in permissions) {
          processPermissions("insert", permissions["insert"]);
        }

        if ("update" in permissions) {
          processPermissions("update", permissions["update"]);
        }

      }, "json" );

      function processPermissions(type, permissions) {
        $.each(permissions, function() {
          addTableSelection(type, tableCounter);
          $('#' + type + '-tables-selection-' + tableCounter + ' option[value="' + this["table"] + '"]').attr("selected", "selected");
          $('#' + type + '-access-selection-' + tableCounter + ' option[value="' + this["access"] + '"]').attr("selected", "selected");

          if (this["access"] == "USER") {
            addUserList(type, tableCounter, this["users"]);
          }

          tableCounter++;
        });
      }

      function createEntries(type) {
        var tables = [];
        var access = [];
        var users = [];

        $('[id^="' + type + '-table-"]').each(function() {
          $(this).find('[id^="' + type + '-tables-selection-"]').each(function() {
            tables.push($(this).val());
          });

          $(this).find('[id^="' + type + '-access-selection-"]').each(function() {
            access.push($(this).val());
          });

          $(this).find('[id^="' + type + '-users-list-"]').each(function() {
            var userList = [];
            $(this).children("input:checked").each(function() {
              userList.push($(this).val());
            });
            users.push(userList);
          });
        });

        var entries = [];
        var userCounter = 0;
        for (var i = 0; i < tables.length; i++) {
          if (access[i] == "USER") {
            var entry = {"table": tables[i], "access": access[i], "users": users[userCounter]};
            userCounter++;
            entries.push(entry);
          } else {
            var entry = {"table": tables[i], "access": access[i]};
            entries.push(entry);
          }
        }

        return entries;
      }

      function getSelectedTables(type) {
        var tables = [];
        var selector = '[id^=\"' + type +'-tables-selection-"]';
        $(selector).each(function() {
          tables.push($(this).val());
        });

        return tables;
      }

      var minus = function ( a, b ) {
          return a.filter(function ( name ) {
              return b.indexOf( name ) === -1;
          });
      };

      function getAllTables() {
        var tables = []
        for (var i = 0; i < infoData.tables.length; i++) {
          tables.push(infoData.tables[i].name);
        }

        return tables;
      }

      function addUserList(type, num, checkedUsers) {
        $("#" + type + "-table-" + num).append("\
          <div id='" + type + "-users-" + num + "' class='control-group'> \
            <label class='control-label' for='field-name'>Users</label> \
            <div id='" + type + "-users-list-" + num + "' class='checked-list'> \
            </div> \
          </div>");

          $.each(userData, function(index, value) {
            var checkBox = $("<input/>").prop("type", "checkbox").val(index);
            if (checkedUsers.includes(index)) {
              checkBox.attr("checked", true);
            }

            $("#" + type + "-users-list-" + num).append(checkBox);
            $("#" + type + "-users-list-" + num).append(value["display_name"]);
            $("#" + type + "-users-list-" + num).append("<br/>");
          });
      }

      function addTableSelection(type, num) {
        var availableTables = minus(getAllTables(), getSelectedTables(type));
        if (availableTables.length == 1) {
          $("#" + type + "-add-table").hide();
        }

        $("#" + type + "-tables").append("<div id='" + type + "-table-" + num + "'></div>");

        $("#" + type + "-table-" + num).append("\
          <div class='control-group control-full'> \
            <label class='control-label' for='field-name'>Table</label> \
            <div class='controls control-group'> \
              <select id='" + type + "-tables-selection-" + num + "'> \
              </select> \
            </div> \
            <div id='" + type + "-remove-table-" + num + "' class='btn btn-danger'>-</div> \
          </div>");

          $("#" + type + "-remove-table-" + num).bind("click", function() {
            $("#" + type + "-table-" + num).remove();
            var availableTables = minus(getAllTables(), getSelectedTables(type));
            if (availableTables.length == 1) {
              $("#" + type + "-add-table").show();
            }
          });

          $.each(availableTables, function() {
            $("#" + type + "-tables-selection-" + num).append($("<option />").val(this).text(this));
          });

          $("#" + type + "-tables-selection-" + num).change(function() {
            console.log($("#" + type + "-tables-selection-" + num).val());
          });

          $("#" + type + "-table-" + num).append("\
            <div class='control-group control-full'> \
              <label class='control-label' for='field-name'>Access</label> \
              <div class='controls'> \
                <select id='" + type + "-access-selection-" + num + "'> \
                  <option value='ALL'>ALL</option> \
                  <option value='AUTH'>AUTH</option> \
                  <option value='NONE'>NONE</option> \
                  <option value='USER'>USER</option> \
                  <option value='OWNER'>OWNER</option> \
                </select> \
              </div> \
            </div>");

          $("#" + type + "-access-selection-" + num).change(function() {
            if ($("#" + type + "-access-selection-" + num).val() == "USER") {
              addUserList(type, num, []);
            } else {
              $("#" + type + "-users-" + num).remove();
            }
          });
      }

      $("#select-add-table").bind("click", function() {
        addTableSelection("select", tableCounter);
        tableCounter++;
      });

      $("#delete-add-table").bind("click", function() {
        addTableSelection("delete", tableCounter);
        tableCounter++;
      });

      $("#insert-add-table").bind("click", function() {
        addTableSelection("insert", tableCounter);
        tableCounter++;
      });

      $("#update-add-table").bind("click", function() {
        addTableSelection("update", tableCounter);
        tableCounter++;
      });
    }
  };
});
