"""Tests for plugin.py."""
import ckan.tests.helpers as helpers
import ckan.tests.factories as factories
import ckan.model as model
from ckanext.welive import utils
import logging
import ckan
import mock
import json
import os
import ConfigParser
import pylons
import datetime

config = ConfigParser.ConfigParser()
config.read(os.environ['CKAN_CONFIG'])

MAIN_SECTION = 'app:main'
SITE_URL = config.get(MAIN_SECTION, 'ckan.site_url')

PLUGIN_SECTION = 'plugin:sparql'
WELIVE_API = config.get(PLUGIN_SECTION, 'welive_api')

AAC_SECTION = 'plugin:authentication'
AAC_URL = config.get(AAC_SECTION, 'aac_url')
CLIENT_ID = config.get(AAC_SECTION, 'client_id')
CLIENT_SECRET = config.get(AAC_SECTION, 'client_secret')

log = logging.getLogger(__name__)

AUTH_TOKEN = 'bearer 2c281152-8593-4d00-8a47-0a75a3f48649'
REFRESH_TOKEN = '448fe59f-5c9a-4c23-88d3-5eb8b34cbbff'

AAC_RESPONSE = json.dumps({
    "access_token": "2c281152-8593-4d00-8a47-0a75a3f48649",
    "token_type": "bearer",
    "refresh_token": "{}".format(REFRESH_TOKEN),
    "expires_in": 36788,
    "scope": "profile.accountprofile.me profile.basicprofile.me",
})

PACKAGE_DATA = {'name': 'test-package', 'title': "Test Package",
                'language': 'es'}

RESOURCE_DATA = {'name': 'Test Resource'}

RESOURCE_URL = 'http://foo/bar/datasetfile.ext'

CSV_DATA = 'ColumnA;ColumnB;ColumnC;\n'\
           '"Row1";0;3\n'\
           '"Row2";5;3\n'

CSV_MAPPING = {
   "mapping": "csv",
   "uri": "{}".format(RESOURCE_URL),
   "key": "_id",
   "delimiter": ";",
   "refresh": 1000
}

JSON_DATA = '{"result": [{"ColumnA": "Row1", "ColumnB": 0, "ColumnC": 3},'\
                        '{"ColumnA": "Row2", "ColumnB": 5, "ColumnC": 3}]}'

JSON_MAPPING = {
   "mapping": "json",
   "uri": "{}".format(RESOURCE_URL),
   "root": "result",
   "key": "_id",
   "refresh": 1000
}


class MockSession(object):

    def __init__(self, data):
        self.session = data

    def __getitem__(self, key):
        return self.session[key]

    def __setitem__(self, key, value):
        self.session[key] = value

    def get(self, key, default):
        if key not in self.session:
            return default
        return self.session[key]

    def save(self):
        pass


class MockResponse(object):
    def __init__(self, content):
        self.content = content

    def json(self):
        return json.loads(self.content)


class TestCreateMapping(object):
    def setup(self):
        model.repo.init_db()
        ckan.plugins.load('welive')

    def teardown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('welive')

    @mock.patch('ckan.logic.action.get.package_show')
    @mock.patch('requests.get')
    def test_create_public_csv_mapping(self, mock_get, mock_package_show):
        mock_get.return_value = MockResponse(CSV_DATA)

        dataset = factories.Dataset()
        mock_package_show.return_value = dataset

        resource = factories.Resource(package_id=dataset['id'], format='csv',
                                      url=RESOURCE_URL)
        mapping = utils.generate_mapping({}, resource)

        assert(json.loads(mapping) == CSV_MAPPING)

        mock_package_show.assert_called_with(mock.ANY,
                                             {'id': resource['package_id']})
        mock_get.assert_called_with(RESOURCE_URL, headers={})

    @mock.patch('ckanext.welive.utils.get_token')
    @mock.patch('ckan.logic.action.get.package_show')
    @mock.patch('requests.get')
    def test_create_private_csv_mapping(self, mock_get, mock_package_show,
                                        mock_get_token):
        mock_get.return_value = MockResponse(CSV_DATA)
        mock_get_token.return_value = AUTH_TOKEN

        organization = factories.Organization()
        dataset = factories.Dataset(private=True, owner_org=organization['id'])
        mock_package_show.return_value = dataset

        resource = factories.Resource(package_id=dataset['id'], format='csv',
                                      url=RESOURCE_URL)
        mapping = utils.generate_mapping({}, resource)

        assert(json.loads(mapping) == CSV_MAPPING)

        mock_package_show.assert_called_with(mock.ANY,
                                             {'id': resource['package_id']})
        mock_get_token.assert_called_with()
        mock_get.assert_called_with(RESOURCE_URL, headers={'Authorization':
                                                           AUTH_TOKEN})

    @mock.patch('ckan.logic.action.get.package_show')
    @mock.patch('requests.get')
    def test_create_public_json_mapping(self, mock_get, mock_package_show):
        mock_get.return_value = MockResponse(JSON_DATA)

        dataset = factories.Dataset()
        mock_package_show.return_value = dataset
        resource = factories.Resource(package_id=dataset['id'], format='json',
                                      url=RESOURCE_URL)
        mapping = utils.generate_mapping({}, resource)

        assert(json.loads(mapping) == JSON_MAPPING)

        mock_package_show.assert_called_with(mock.ANY,
                                             {'id': resource['package_id']})
        mock_get.assert_called_with(RESOURCE_URL, headers={})

    @mock.patch('ckanext.welive.utils.get_token')
    @mock.patch('ckan.logic.action.get.package_show')
    @mock.patch('requests.get')
    def create_private_json_mapping(self, mock_get, mock_package_show,
                                    mock_get_token):
        mock_get.return_value = MockResponse(JSON_DATA)
        mock_get_token.return_value = AUTH_TOKEN

        organization = factories.Organization()
        dataset = factories.Dataset(private=True, owner_org=organization['id'])
        mock_package_show.return_value = dataset
        resource = factories.Resource(package_id=dataset['id'], format='json',
                                      url=RESOURCE_URL)
        mapping = utils.generate_mapping({}, resource)

        assert(json.loads(mapping) == JSON_MAPPING)

        mock_package_show.assert_called_with(mock.ANY,
                                             {'id': resource['package_id']})
        mock_get_token.assert_called_with()
        mock_get.assert_called_with(RESOURCE_URL, headers={'Authorization':
                                                           AUTH_TOKEN})

    @mock.patch('ckan.logic.action.get.package_show')
    @mock.patch('requests.get')
    def test_create_public_rdf_mapping(self, mock_get, mock_package_show):
        mock_get.return_value = MockResponse('{}')

        dataset = factories.Dataset()
        mock_package_show.return_value = dataset
        resource = factories.Resource(package_id=dataset['id'], format='rdf',
                                      url=RESOURCE_URL)
        utils.generate_mapping({}, resource)

        mock_package_show.assert_called_with(mock.ANY,
                                             {'id': resource['package_id']})
        mock_get.assert_called_with(
            '{}sparql-query-maker/mapping'.format(WELIVE_API),
            params={'sparqlEndpoint': '{}/dataset/{}/sparql'.format(
                SITE_URL, resource['package_id'])},
            headers={}
        )

    @mock.patch('ckanext.welive.utils.get_token')
    @mock.patch('ckan.logic.action.get.package_show')
    @mock.patch('requests.get')
    def create_private_rdf_mapping(self, mock_get, mock_package_show,
                                   mock_get_token):
        mock_get.return_value = MockResponse('{}')
        mock_get_token.return_value = AUTH_TOKEN

        dataset = factories.Dataset(private=True)
        mock_package_show.return_value = dataset
        resource = factories.Resource(package_id=dataset['id'], format='rdf',
                                      url=RESOURCE_URL)
        utils.generate_mapping({}, resource)

        mock_package_show.assert_called_with(mock.ANY,
                                             {'id': resource['package_id']})

        mock_get_token.assert_called_with()
        mock_get.assert_called_with(
            '{}sparql-query-maker/mapping'.format(WELIVE_API),
            params={'sparqlEndpoint': '{}/dataset/{}/sparql'.format(
                SITE_URL, resource['package_id'])},
            headers={'Authorization': AUTH_TOKEN}
        )


class TestGetToken(object):

    @mock.patch('pylons.session', new=MockSession)
    @mock.patch('requests.post')
    def test_get_expired_token(self, mock_post):
        mock_post.return_value = MockResponse(AAC_RESPONSE)
        pylons.session = MockSession({'ckanext-welive-refresh': REFRESH_TOKEN})
        token = utils.get_token()
        assert(token == AUTH_TOKEN)
        mock_post.assert_called_with(
            '{}/oauth/token'.format(AAC_URL),
            params={'refresh_token': REFRESH_TOKEN,
                    'client_id': CLIENT_ID,
                    'client_secret': CLIENT_SECRET,
                    'grant_type': 'refresh_token'}
        )

    @mock.patch('pylons.session', new=MockSession)
    @mock.patch('requests.post')
    def test_get_not_expired_token(self, mock_post):
        mock_post.return_value = MockResponse(AAC_RESPONSE)
        pylons.session = MockSession(
            {'ckanext-welive-refresh': REFRESH_TOKEN,
             'ckanext-welive-expiration':
             datetime.datetime.now() + datetime.timedelta(seconds=37000),
             'ckanext-welive-token': AUTH_TOKEN}
        )
        token = utils.get_token()
        assert(token == AUTH_TOKEN)
        mock_post.assert_not_called()
