import ckan.plugins as p
import ckan.plugins.toolkit as tk
import logging
import ConfigParser
import os
import requests

config = ConfigParser.ConfigParser()
config.read(os.environ['CKAN_CONFIG'])

WELIVE_SECTION = 'plugin:welive_utils'
WELIVE_API = config.get(WELIVE_SECTION, 'welive_api')


log = logging.getLogger(__name__)


def is_mapped(resource_dict):
    if 'mapping' in resource_dict:
        if resource_dict['mapping'] not in ['', None, '{}']:
            return True

    return False


def resource_mapped(resource):
    response = requests.get(
        '{}/ods/get-status/{}'.format(WELIVE_API, resource['id'])).json()
    if response.get('message', '') == 'Job correctly executed':
        return True
    return False


def dataset_mapped(package):
    for resource in package.get('resources', []):
        response = requests.get(
            '{}/ods/get-status/{}'.format(WELIVE_API, resource['id'])).json()
        if response.get('message', '') == 'Job correctly executed':
            return True
    return False


class WelivePlugin(p.SingletonPlugin, tk.DefaultDatasetForm):
    p.implements(p.IConfigurer)
    p.implements(p.IRoutes, inherit=True)
    p.implements(p.IDatasetForm)
    p.implements(p.ITemplateHelpers)

    def before_map(self, map):
        map.connect('/dataset/{id}/permissions_edit/{resource_id}',
                    controller='ckanext.welive.controller:WeLiveController',
                    action='edit_permissions')

        map.connect('/dataset/{id}/reset_data/{resource_id}',
                    controller='ckanext.welive.controller:WeLiveController',
                    action='reset_data')

        return map

    # IConfigurer

    def update_config(self, config):
        tk.add_template_directory(config, 'templates')
        tk.add_resource('fanstatic', 'welive')

    # IDatasetForm

    def _modify_package_schema(self, schema):
        schema['resources'].update({
            'mapping': [tk.get_validator('ignore_missing')],
            'permissions': [tk.get_validator('ignore_missing')]
        })

        return schema

    def update_package_schema(self):
        schema = super(WelivePlugin, self).update_package_schema()
        schema = self._modify_package_schema(schema)

        return schema

    def show_package_schema(self):
        schema = super(WelivePlugin, self).show_package_schema()
        schema = self._modify_package_schema(schema)

        return schema

    def is_fallback(self):
        return True

    def package_types(self):
        return []

    # ITemplateHelpers

    def get_helpers(self):
        return {'is_mapped': is_mapped,
                'dataset_mapped': dataset_mapped,
                'resource_mapped': resource_mapped}
