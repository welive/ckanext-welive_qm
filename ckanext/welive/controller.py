from ckan.controllers.package import PackageController
from ckan.plugins import toolkit as tk
import ckan.logic as logic
import ckan.model as model
from pylons import request
import ckan.lib.helpers as h
import json
import ConfigParser
import os
import requests
import pylons

config = ConfigParser.ConfigParser()
config.read(os.environ['CKAN_CONFIG'])

PLUGIN_SECTION = 'plugin:welive_utils'
WELIVE_API = config.get(PLUGIN_SECTION, 'welive_api')

render = tk.render
get_action = logic.get_action
c = tk.c
check_access = logic.check_access

class WeLiveController(PackageController):

    def edit_permissions(self, id, resource_id):
        context = {'model': model, 'session': model.Session,
                   'api_version': 3, 'for_edit': True,
                   'user': c.user or c.author, 'auth_user_obj': c.userobj}
        pkg_dict = get_action('package_show')(context, {'id': id})

        resource_dict = get_action('resource_show')(context, {'id': resource_id})
        c.pkg_dict = pkg_dict
        c.resource = resource_dict

        if 'permissions' in request.params:
            resource_dict['permissions'] = request.params['permissions']
            get_action('resource_update')(context, resource_dict)

        user_list = get_action('user_list')(context, {})
        user_data = {}
        for user in user_list:
            user_id = user['name']
            if user_id.isdigit():
                user_id = str(int(user_id))

            user_data[user_id] = {
                'display_name': user['display_name']
            }

        c.user_data = json.dumps(user_data)

        c.welive_api = WELIVE_API

        return render('package/permissions_edit.html')

    def reset_data(self, id, resource_id):
        reset_url = WELIVE_API + '/dataset/%s/resource/%s/reset' % (id, resource_id)
        headers = {'Authorization': pylons.session['ckanext-welive-token']}

        r = requests.get(reset_url, headers=headers)

        return r.text
