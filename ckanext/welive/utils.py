import requests
import csv
import json
import logging
import ConfigParser
import os
import pylons
import datetime
from ckan.logic.action import get
from urlparse import urlparse

log = logging.getLogger(__name__)

config = ConfigParser.ConfigParser()
config.read(os.environ['CKAN_CONFIG'])

MAIN_SECTION = 'app:main'
SITE_URL = config.get(MAIN_SECTION, 'ckan.site_url')
STORAGE_PATH = config.get(MAIN_SECTION, 'ckan.storage_path')

PLUGIN_SECTION = 'plugin:sparql'
WELIVE_API = config.get(PLUGIN_SECTION, 'welive_api')

JSON_FORMAT = ['json', 'application/json']
CSV_FORMAT = ['csv', 'text/comma-separated-values', 'text/csv',
              'application/csv']
RDF_FORMAT = ['rdf', 'application/rdf+xml', 'text/plain',
              'application/x-turtle', 'text/rdf+n3']


AAC_SECTION = 'plugin:authentication'
AAC_URL = config.get(AAC_SECTION, 'aac_url')
CLIENT_ID = config.get(AAC_SECTION, 'client_id')
CLIENT_SECRET = config.get(AAC_SECTION, 'client_secret')


def get_local(uri):
    if '#' in uri:
        return uri.split('#')[1]
    else:
        return uri.split('/')[-1]


def get_token():
    expiration_date = pylons.session.get('ckanext-welive-expiration', None)
    if expiration_date is not None and expiration_date > datetime.datetime.now():
        return pylons.session.get('ckanext-welive-token', None)
    else:
        response = requests.post(
            '%s/oauth/token' % AAC_URL,
            params={'refresh_token':
                    pylons.session['ckanext-welive-refresh'],
                    'client_id': CLIENT_ID,
                    'client_secret': CLIENT_SECRET,
                    'grant_type': 'refresh_token'}).json()
        pylons.session['ckanext-welive-token'] = '%s %s' % (response['token_type'], response['access_token'])
        expiration_date = datetime.datetime.now() + datetime.timedelta(0, response['expires_in'])
        pylons.session['ckanext-welive-expiration'] = expiration_date
        pylons.session.save()
        return pylons.session['ckanext-welive-token']


def generate_mapping(context, resource):
    log.debug('Generating mapping for resource {}'.format(resource['id']))
    package = None
    mapping = {}
    if 'package_id' not in resource:
        try:
            resource_dict = get.resource_show(context, {'id':
                                                        resource['id']})
            package = get.package_show(context, {'id':
                                                 resource_dict['package_id']})
        except Exception as e:
            log.debug(e)

    else:
        try:
            package = get.package_show(context, {'id': resource['package_id']})
        except Exception as e:
            log.debug(e)
            log.debug('Error when getting dataset for resource {}'.format(
                      resource['id']))
    if package is not None:
        private = package['private']
        headers = {}
        if private:
            headers['Authorization'] = get_token()
        try:
            if 'format' in resource:
                _format = None
                if resource['format'].lower() in JSON_FORMAT:
                    _format = 'json'
                elif resource['format'].lower() in CSV_FORMAT:
                    _format = 'csv'
                elif resource['format'].lower() in RDF_FORMAT:
                    _format = 'rdf'
                log.debug('Resource format: {}'.format(_format))
                if _format is not None:
                    if _format in ['csv', 'json']:
                        mapping = {'mapping': _format,
                                   'uri': resource['url'],
                                   'key': '_id',
                                   'refresh': 1000}
                        url = resource['url']
                        parsed_url = urlparse(url)
                        if parsed_url.netloc == urlparse(SITE_URL).netloc:
                            resource_path = os.path.join(
                                STORAGE_PATH, 'resources', resource['id'][:3],
                                resource['id'][3:6], resource['id'][6:])
                            f = open(resource_path, 'r')
                            content = f.read()
                        else:
                            response = requests.get(resource['url'], headers=headers)
                            content = response.content
                            log.debug('Response')
                            log.debug(response)
                        if _format == 'csv':
                            dialect = csv.Sniffer().sniff(content)
                            mapping['delimiter'] = dialect.delimiter
                        elif _format == 'json':
                            json_response = json.loads(content)
                            if type(json_response) == list:
                                mapping['root'] = "/"
                            else:
                                log.debug(json_response.keys())
                                mapping['root'] = json_response.keys()[0]
                    elif _format == 'rdf':
                        sparql_endpoint = '%s/dataset/%s/sparql' % (SITE_URL, resource['package_id'])
                        mapping = requests.get(
                            '%s/sparql-query-maker/mapping' % WELIVE_API,
                            params={'sparqlEndpoint': sparql_endpoint}, headers=headers
                        ).json()

        except Exception as e:
            log.debug('Error when parsing resource %s' % resource)
            log.debug(e)

    return json.dumps(mapping)
